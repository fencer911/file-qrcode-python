package niu.fencer.tool.qrcode.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Random;

import niu.fencer.tool.qrcode.R;

public class QuickItemAdapter extends RecyclerView.Adapter<QuickItemViewHolder> {
    private Random random;

    public QuickItemAdapter(int seed) {
        this.random = new Random(seed);
    }

//    @Override
//    public int getItemViewType(final int position) {
////        return R.layout.fragment_quick_item;
//    }

    @NonNull
    @Override
    public QuickItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new QuickItemViewHolder(view);
    }
    @Override
    public int getItemCount() {
        return 100;
    }
    @Override
    public void onBindViewHolder(@NonNull QuickItemViewHolder holder, int position) {
        holder.getView().setText(String.valueOf(random.nextInt()));
    }


}