package niu.fencer.tool.qrcode.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import niu.fencer.tool.qrcode.R;
import niu.fencer.tool.qrcode.util.ActivityUtils;
import niu.fencer.tool.qrcode.util.FileUtil;
import niu.fencer.tool.qrcode.util.LogUtil;
import niu.fencer.tool.qrcode.util.PermissionChecker;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class FragmentFile extends Fragment {
    public static final File VIRTUAL_STORAGE_RECENTS = new File("/storage/recent-files");
    public static final File VIRTUAL_STORAGE_FAVOURITE = new File("/storage/favourite-files");
    public static final File VIRTUAL_STORAGE_POPULAR = new File("/storage/popular-files");
    public static final File VIRTUAL_STORAGE_APP_DATA_PRIVATE = new File("/storage/appdata-private");

    private RecyclerView fileListView;
    public FileItemAdapter itemAdapter;


    private static String [] CMMON_FOLDER_NAME =null;
    private static List<File> CMMON_FOLDER =new ArrayList<>();
    private Spinner spinner = null;
    private ArrayAdapter<String> adapter = null;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_file, container, false);
        spinner = (Spinner)root.findViewById(R.id.spinner);
//        filePath=(TextView)root.findViewById(R.id.filePath) ;
//        CMMON_FOLDER_NAME =this.getResources().getStringArray(R.array.common_folers);

//        if (new PermissionChecker(FragmentFile.this.getActivity()).doIfExtStoragePermissionGranted()){
//            itemAdapter.loadFolder(Environment.getRootDirectory());
//        }else{
//            Toast.makeText(this.getContext(), "权限不足!", Toast.LENGTH_SHORT).show();
//        }

        fileListView = (RecyclerView) root.findViewById(R.id.fileList);
        LinearLayoutManager lam = (LinearLayoutManager) fileListView.getLayoutManager();
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), lam.getOrientation());
        fileListView.addItemDecoration(dividerItemDecoration);
        File appDir=new ActivityUtils(getActivity()).getAppDataPrivateDir();
        itemAdapter=new FileItemAdapter(this,getContext(),appDir);
        fileListView.setAdapter(itemAdapter);
        setHasOptionsMenu(true);
        return root;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.filesystem__menu, menu);
    }

    //当OptionsMenu被选中的时候处理具体的响应事件
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        Toast.makeText(getContext(),"onOptionsItemSelected item file:"+item.getTitle(),Toast.LENGTH_SHORT).show();
        switch (item.getGroupId()) {
            case R.id.action_go_to: {
                break;
            }
            case R.id.action_qrcode_scan:{
                break;
            }
            default:
                ActivityUtils actUtil = new ActivityUtils(getActivity());
                File folder = actUtil.getFolderToLoadByMenuId(item.getItemId());
                LogUtil.i("folder:" + folder.getAbsolutePath());
                itemAdapter.loadFolder(folder);
        }
        return super.onOptionsItemSelected(item);
    }


    public static Fragment newInstance() {
        return new FragmentFile();
    }

}