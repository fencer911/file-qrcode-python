package niu.fencer.tool.qrcode.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.Log;

import niu.fencer.tool.qrcode.util.ActivityUtils;
import niu.fencer.tool.qrcode.util.LogUtil;

public class MyEditText extends androidx.appcompat.widget.AppCompatEditText {
    static final int ID_PASTE = android.R.id.paste;

    public MyEditText(Context context) {
        super(context);
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTextContextMenuItem(int id) {
        LogUtil.i("onTextContextMenuItem id:" + id + ",ID_PASTE:" + ID_PASTE);
        if (ID_PASTE == id) {
            this.setText(ActivityUtils.getClipText(this.getContext()));
            return true;
        } else {
            return super.onTextContextMenuItem(id);
        }
    }
}