package niu.fencer.tool.qrcode.ui;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import niu.fencer.tool.qrcode.R;

public class QuickItemViewHolder extends RecyclerView.ViewHolder {

    private TextView view;
    public QuickItemViewHolder(@NonNull View itemView) {
        super(itemView);
//        view = itemView.findViewById(R.id.randomText);
    }

    public TextView getView(){
        return view;
    }
}