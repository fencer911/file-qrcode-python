package niu.fencer.tool.qrcode.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.FileUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.zxing.common.StringUtils;

import java.io.File;
import java.util.Date;

import niu.fencer.tool.qrcode.R;
import niu.fencer.tool.qrcode.activity.MainActivity;
import niu.fencer.tool.qrcode.activity.QrCodeGenActivity;
import niu.fencer.tool.qrcode.util.ActivityUtils;
import niu.fencer.tool.qrcode.util.FileUtil;
import niu.fencer.tool.qrcode.util.LogUtil;
import niu.fencer.tool.qrcode.util.StringUtil;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;


public class FragmentText extends Fragment {

    private File curFile;
    private MyEditText _doc;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_text, container, false);
        _doc = root.findViewById(R.id.docEditText);
        _doc.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        setHasOptionsMenu(true);

        FloatingActionButton fabPaste =root.findViewById(R.id.pasteFab);
        fabPaste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _doc.setText(ActivityUtils.getClipText(getContext()));
            }
        });
        FloatingActionButton sendFab =root.findViewById(R.id.sendFab);
        sendFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity act=(MainActivity)getActivity();
                boolean isFile=curFile==null?false:true;
                String source="";
                if(isFile){
                    source=curFile.getAbsolutePath();
                }else{
                    source=_doc.getText().toString();
                }
                startQRCode(act,isFile,source);
            }
        });

        return root;
    }
    private void startQRCode(MainActivity act , boolean isFile, String source){
        Intent intent = new Intent(act, QrCodeGenActivity.class);
        intent.putExtra(MainActivity.IS_FILE,isFile);
        intent.putExtra(MainActivity.QRCODE_SOURCE,source);
        act.startActivity(intent);
    }
    public void switchToMe(Bundle args){
        String source = args!= null ? args.getString(MainActivity.QRCODE_SOURCE, "") : "";
        boolean isFile=args.getBoolean(MainActivity.IS_FILE);
        if(isFile){
            LogUtil.i("selectedFile="+source);
            if(args.getBoolean(MainActivity.IN_PRIVATE_DIR)){
                _doc.setText(FileUtil.readPackageFile(getContext(),source));
                this.getActivity().setTitle(source);
            }else {
                File selectedFileObj = new File(source);
                this.getActivity().setTitle(selectedFileObj.getName());
                if (selectedFileObj.exists() && selectedFileObj.canRead()) {
                    LogUtil.i("selectedFile=" + source + " can read");
                    curFile = selectedFileObj;
                    byte[] docText = FileUtil.readFileFast(selectedFileObj);
                    _doc.setText(new String(docText));
                } else {
                    LogUtil.i("selectedFile=" + source + " can not read");
                    Toast.makeText(getContext(), source + "权限不足!", Toast.LENGTH_SHORT).show();
                }
            }
        }else{
            _doc.setText(source);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.text_menu, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        Toast.makeText(getContext(),"onOptionsItemSelected item text:"+item.getTitle(),Toast.LENGTH_SHORT).show();
        String text=_doc.getText().toString();
        switch (item.getItemId()) {
            case R.id.action_save: {

                if(curFile==null){
                    String fileName=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())+".txt";;
                    LogUtil.i(fileName+"save in private !");
                    FileUtil.savePackageFile(getContext(),fileName,text, Context.MODE_PRIVATE);
//                    Toast.makeText(getContext(), fileName+"save in private !", Toast.LENGTH_SHORT).show();
                }else{
                    LogUtil.i(curFile.getAbsolutePath()+"will be write");
                    FileUtil.writeFile(curFile,text.getBytes());
                }
                break;
            }
            case R.id.action_new:{
                curFile=null;
                LogUtil.i("action_new");
                _doc.setText("");
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }
    public static Fragment newInstance() {
        return new FragmentText();
    }
}