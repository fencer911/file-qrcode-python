package niu.fencer.tool.qrcode.activity;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import niu.fencer.tool.qrcode.R;

/**
 * @Author: wuchaowen
 * @Description:
 * @Time:
 **/
public class QrCodeGenStyleAdapter extends RecyclerView.Adapter<QrCodeGenStyleAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private ViewPager2 viewPager2;

    private int[] imageArray = new int[]{R.drawable.documentnew,R.drawable.documentopen,R.drawable.documentsave};

    public QrCodeGenStyleAdapter(Context context, ViewPager2 viewPager2) {
        this.mInflater = LayoutInflater.from(context);
        this.viewPager2 = viewPager2;
    }

    @NonNull
    @Override
    public QrCodeGenStyleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.activity_qr_code_gen_style, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QrCodeGenStyleAdapter.ViewHolder holder, int position) {
        holder._imageView.setImageResource(imageArray[position]);
    }

    @Override
    public int getItemCount() {
        return imageArray.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView _imageView;
        ViewGroup _layout;

        ViewHolder(View itemView) {
            super(itemView);
            _imageView = itemView.findViewById(R.id.imageView);
            _layout = itemView.findViewById(R.id.container);

        }
    }
}
