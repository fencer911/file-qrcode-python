package niu.fencer.tool.qrcode.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import niu.fencer.tool.qrcode.R;

public class FileItemViewHolder extends RecyclerView.ViewHolder {
    public ViewGroup itemRoot;
    public ImageView image;
    public TextView title;
    public TextView description;
    public View genButton;
    FileItemViewHolder(View itemView) {
        super(itemView);
        itemRoot = itemView.findViewById(R.id.ui__filesystem_item__root);
        image = itemView.findViewById(R.id.ui__filesystem_item__image);
        title=itemView.findViewById(R.id.ui__filesystem_item__title);
        description=itemView.findViewById(R.id.ui__filesystem_item__description);
        genButton=itemView.findViewById(R.id.genButton);
    }
}