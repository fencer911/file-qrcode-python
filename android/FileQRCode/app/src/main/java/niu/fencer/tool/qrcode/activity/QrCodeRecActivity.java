/*
 * Copyright (C) 2018 Jenly Yu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package niu.fencer.tool.qrcode.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.google.zxing.Result;
import com.king.zxing.CameraConfig;
import com.king.zxing.CaptureActivity;

import com.king.zxing.DecodeConfig;
import com.king.zxing.DecodeFormatManager;
import com.king.zxing.analyze.MultiFormatAnalyzer;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import niu.fencer.tool.qrcode.R;
import niu.fencer.tool.qrcode.util.Decoder;
import niu.fencer.tool.qrcode.util.FileUtil;
import niu.fencer.tool.qrcode.util.GZipUtil;
import niu.fencer.tool.qrcode.util.LogUtil;
import niu.fencer.tool.qrcode.util.StatusBarUtils;

/**
 * 自定义继承CaptureActivity
 * @author Jenly <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
public class QrCodeRecActivity extends CaptureActivity {

    private boolean isContinuousScan;
    private Decoder decoder;
    private TextView tvTitle;

    @Override
    public int getLayoutId() {
        return R.layout.activity_qr_code_rec;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        tvTitle = findViewById(R.id.tvTitle);
        decoder =new Decoder();
        isContinuousScan =true;// getIntent().getBooleanExtra(MainActivity.KEY_IS_CONTINUOUS,false);
    }

    @Override
    public void initCameraScan() {
        super.initCameraScan();
        //初始化解码配置
        DecodeConfig decodeConfig = new DecodeConfig();
        decodeConfig.setHints(DecodeFormatManager.ALL_HINTS)////设置解码
                .setSupportVerticalCode(true)//设置是否支持扫垂直的条码 （增强识别率，相应的也会增加性能消耗）
                .setSupportLuminanceInvert(true)//设置是否支持识别反色码，黑白颜色反转（增强识别率，相应的也会增加性能消耗）
                .setAreaRectRatio(0.8f)//设置识别区域比例，默认0.8，设置的比例最终会在预览区域裁剪基于此比例的一个矩形进行扫码识别
//                .setAreaRectVerticalOffset(0)//设置识别区域垂直方向偏移量，默认为0，为0表示居中，可以为负数
//                .setAreaRectHorizontalOffset(0)//设置识别区域水平方向偏移量，默认为0，为0表示居中，可以为负数
                .setFullAreaScan(false);//设置是否全区域识别，默认false

        //获取CameraScan，里面有扫码相关的配置设置。CameraScan里面包含部分支持链式调用的方法，即调用返回是CameraScan本身的一些配置建议在startCamera之前调用。
        getCameraScan().setPlayBeep(false)//设置是否播放音效，默认为false
                .setVibrate(true)//设置是否震动，默认为false
                .setCameraConfig(new CameraConfig())//设置相机配置信息，CameraConfig可覆写options方法自定义配置
                .setNeedAutoZoom(false)//二维码太小时可自动缩放，默认为false
                .setNeedTouchZoom(true)//支持多指触摸捏合缩放，默认为true
                .setDarkLightLux(45f)//设置光线足够暗的阈值（单位：lux），需要通过{@link #bindFlashlightView(View)}绑定手电筒才有效
                .setBrightLightLux(100f)//设置光线足够明亮的阈值（单位：lux），需要通过{@link #bindFlashlightView(View)}绑定手电筒才有效
                .bindFlashlightView(ivFlashlight)//绑定手电筒，绑定后可根据光线传感器，动态显示或隐藏手电筒按钮
                .setOnScanResultCallback(this)//设置扫码结果回调，需要自己处理或者需要连扫时，可设置回调，自己去处理相关逻辑
                .setAnalyzer(new MultiFormatAnalyzer(decodeConfig))//设置分析器,DecodeConfig可以配置一些解码时的配置信息，如果内置的不满足您的需求，你也可以自定义实现，
                .setAnalyzeImage(true)//设置是否分析图片，默认为true。如果设置为false，相当于关闭了扫码识别功能
                .startCamera();//启动预览
    }

    /**
     * 扫码结果回调
     * @param result 扫码结果
     * @return
     */
    @Override
    public boolean onScanResultCallback(Result result) {
        if(isContinuousScan) {//连续扫码时，直接弹出结果
            try {
                if (this.decoder.receiveAll) {
                    Bundle args=new Bundle();
                    args.putBoolean(MainActivity.IS_FILE,false);
                    if(this.decoder.size==0){
                        args.putString(MainActivity.QRCODE_SOURCE,result.getText());
                    }else{
                        byte[] data=this.decoder.toByteArray();
                        if(this.decoder.fileName.startsWith("gz1"))
                            data=GZipUtil.uncompress(data);
                        String fileName=this.decoder.fileName.substring(3);
                        if(this.decoder.NO_NAME.equals(fileName)){
                            args.putString(MainActivity.QRCODE_SOURCE,new String(data,"UTF-8"));
                        }else{
//                            ContextCompat.getExternalFilesDirs(getBaseContext(),Environment.
                            args.putString(MainActivity.QRCODE_SOURCE,fileName);
                            args.putBoolean(MainActivity.IS_FILE,true);
                            args.putBoolean(MainActivity.IN_PRIVATE_DIR,true);
                            FileUtil.savePackageFile(getBaseContext(),fileName, data,Context.MODE_PRIVATE);
                        }
                    }
                    Intent intent = getIntent();
                    intent.putExtras(args);
                    setResult(MainActivity.REQUEST_CODE_SCAN,intent);
                    finish();

                }else{
                    this.decoder.receive(result.getText());
                    String tips=this.decoder.total+"/"+this.decoder.ids.size()+"["+this.decoder.curIndex + "]";
                    this.tvTitle.setText(tips);
                }
            } catch (Throwable e) {
                e.printStackTrace();
                LogUtil.e(e.getMessage(),e);
                Toast.makeText(this, "Error"+e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
        return true;
    }


    public void onClick(View v){
        switch (v.getId()){
            case R.id.ivLeft:
                onBackPressed();
                break;
        }
    }
}