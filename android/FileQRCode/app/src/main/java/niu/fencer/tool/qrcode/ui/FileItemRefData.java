/*#######################################################
 *
 *   Maintained by Gregor Santner, 2017-
 *   https://gsantner.net/
 *
 *   License: Apache 2.0 / Commercial
 *  https://github.com/gsantner/opoc/#licensing
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
#########################################################*/
package niu.fencer.tool.qrcode.ui;


import android.os.Environment;


import java.io.File;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import niu.fencer.tool.qrcode.R;

@SuppressWarnings({"unused", "WeakerAccess"})
public class FileItemRefData {
    public static class Options implements Serializable {

        public File rootFolder = Environment.getExternalStorageDirectory(), mountedStorageFolder = null;
        public String requestId = "show_dialog";


        // Dialog type
        public boolean
                doSelectFolder = true,
                doSelectFile = false,
                doSelectMultiple = false;

        public boolean mustStartWithRootFolder = true,
                folderFirst = true,
                descModtimeInsteadOfParent = false,
                showDotFiles = true;

        public int itemSidePadding = 16; // dp

        // Visibility of elements
        public boolean
                titleTextEnable = true,
                utilsBarEnable = true,
                searchEnable = true,
                upButtonEnable = true,
                homeButtonEnable = true,
                cancelButtonEnable = true,
                okButtonEnable = true;

        public Comparator<File> fileComparable = null;
        public Function<File, Boolean> fileOverallFilter = input -> true;


        public int cancelButtonText = android.R.string.cancel;

        public int okButtonText = android.R.string.ok;

        public int titleText = android.R.string.untitled;

        public int searchHint = android.R.string.search_go;

        public int contentDescriptionFolder = 0;

        public int contentDescriptionSelected = 0;

        public int contentDescriptionFile = 0;

        public int homeButtonImage = android.R.drawable.star_big_on;

        public int searchButtonImage = android.R.drawable.ic_menu_search;

        public int folderImage = R.drawable.folder;

        public int selectedItemImage = android.R.drawable.checkbox_on_background;

        public int fileImage = R.drawable.file;

        public int backgroundColor = android.R.color.background_light;

        public int primaryColor = 0;

        public int accentColor = 0;

        public int primaryTextColor = 0;

        public int secondaryTextColor = 0;

        public int titleTextColor = 0;

        public List<File> favouriteFiles, recentFiles, popularFiles = null;
    }

}
