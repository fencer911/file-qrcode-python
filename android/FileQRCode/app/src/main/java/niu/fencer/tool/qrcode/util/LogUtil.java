package niu.fencer.tool.qrcode.util;

import android.util.Log;

public class LogUtil {
    static String tag="9527";
    static String tagError="9527Error";
    public static void e(String msg){
        Log.e(tagError,msg);
    }
    public static void e(String msg,Throwable e){
        Log.e(tagError,msg,e);
    }
    public static void i(String msg){
        Log.i(tag,msg);
    }
}
