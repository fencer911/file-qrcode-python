# FileQRCodePython

#### 介绍
文件二维码 利用二维码的动态生成与识别来传输数据（短文本，长文本，文件）， 理论大小不限，但越大越慢，小文件是可以接受的 有java与python实现版 。

本版本同时包含安卓版本的实现，能和Python版的程序能够互传文件(无网:截屏或摄像头)

#### 软件架构
python 一个python版本文件二维码动态生成与识别

android 一个安卓版本文件二维码动态生成与识别.(文本接受完后立即打开显示，其他类型文件放到私有目录，不打开)

**文件介绍：**

python/fencer/fileqrcode.py   主程序,有注册模块(就是一个加密)

python/fencer/fileqrcode.ui   界面Qt Designer 文件  [参考](https://blog.csdn.net/Rozol/article/details/87705426)





#### 安装教程

**python版：**

pip install numpy

pip install pyzbar

pip install opencv-python

pip install pyqt5

pip install pillow

pip install wmi

pip install pydes

pip install cacheout

打包exe:(要调整一下路径,我是把ui部分放到主程序合并)

```bat
pyinstaller fileqrcode.spec
```

#### 使用说明

有打包好的PC端 exe程序，和安卓apk(有小bug:静态二维识别有点问题<源码已修正>)

使用参考:

   https://blog.csdn.net/fencer911/article/details/113391674?spm=1001.2014.3001.5501

**警告说明：**

**不要应用于非法目的。**

#### 参与贡献




#### 点个赞吧!

